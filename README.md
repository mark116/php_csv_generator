
> ## Class_Csv.php
> 
> 1.   author: mark davis
> 2.   bitbucket username: mark116
> 3.   an example file is additionally in the repo that has an example of how to create a csv with php array


#HOW TO USE################################################

    include_once("Class_Csv.php");
    echo "<h1>Csv Example with Class_Csv.php </h1>";
    ini_set("display_errors","On");
    $header_arr = array("first_name","last_name", "occupation", "age");
    print_r($header_arr);

    $rows = array();
    $rows[] = array("first_name"=>"mark", "last_name"=>"davis", "occupation"=>"web developer", "age"=>27);
    $rows[] = array("first_name"=>"john", "last_name"=>"rogers", "occupation"=>"phd student", "age"=>26);
    $rows[] = array("first_name"=>"alex", "last_name"=>"poopersnapper", "occupation"=>"jerkface", "age"=>250);


    print_r($rows);

    // set this to br if you want to see it pretty in the browser
    //$csv_obj = new $csv($header_arr, $rows, "<br />");
    $csv_obj = new Csv($header_arr, $rows);
    $csv_obj->setLineSeparator("<br />");
    $csv_str = $csv_obj->createCsvString();



    echo $csv_str;

# BELOW IS THE RESULTING OUTPUT FROM THE CODE ABOVE AS (SEEN WITH VIEW SOURCE ON A BROWSER)
    <h1>Csv Example with Class_Csv.php </h1>Array
    (
        [0] => first_name
        [1] => last_name
        [2] => occupation
        [3] => age
    )
    Array
    (
        [0] => Array
            (
                [first_name] => mark
                [last_name] => davis
                [occupation] => web developer
                [age] => 27
            )

        [1] => Array
            (
                [first_name] => john
                [last_name] => rogers
                [occupation] => phd student
                [age] => 26
            )

        [2] => Array
            (
                [first_name] => alex
                [last_name] => poopersnapper
                [occupation] => jerkoff
                [age] => 250
            )

    )
    first_name,last_name,occupation,age,<br />mark,davis,web developer,27<br />john,rogers,phd student,26<br />alex,poopersnapper,jerkoff,250`

# HOW TO SET IT TO DOWNLOAD FROM THE BROWSER
    $rows = array();
    $rows[] = array("name"=>"John", "age"=>"100");
    $rows[] = array("name"=>"Catherine", "age"=>"25");
    // do not echo anything above here, becasue php header() is called in download, and your download will fail
    $csv = new Csv(array("name,age"), $rows);
    $csv_obj->download("name_your_file_here");


