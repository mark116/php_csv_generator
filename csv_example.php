<?php

include_once("Class_Csv.php");
echo "<h1>Csv Example with Class_Csv.php </h1>";
ini_set("display_errors","On");
$header_arr = array("first_name","last_name", "occupation", "age");
print_r($header_arr);

$rows = array();
$rows[] = array("first_name"=>"mark", "last_name"=>"davis", "occupation"=>"web developer", "age"=>27);
$rows[] = array("first_name"=>"nina", "last_name"=>"brahme", "occupation"=>"phd student", "age"=>26);

print_r($rows);

// set this to br if you want to see it pretty in the browser
//$csv_obj = new $csv($header_arr, $rows, "<br />");
$csv_obj = new Csv($header_arr, $rows);
$csv_obj->setLineSeparator("<br />");
$csv_str = $csv_obj->createCsvString();

echo $csv_str;

