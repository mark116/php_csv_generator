<?php

class Csv{
	public $header_arr;
	public $rows;
	public $line_separator;
	public function __construct($header_array, $rows){
		$this->rows = $rows;
		$this->header_arr = $header_array;
		$this->setLineSeparator("\n");
	}
	public function download($filename){
		header("Content-type: text/csv");
		$date = date("Y-m-d");
		header("Content-Disposition: attachment; filename=$filename_$date.csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		echo $this->createCsvString();
		exit();
	}
	public function createCsvString(){
		$headers = $this->header_arr;
		$rows = $this->rows;
		$csv = "";
		$header_count = count($headers);
		foreach($headers as $h_key => $header){
			
			if($h_key  == ($header_count - 1)){
				$csv .= "$header" . ",";
			}else{
				$csv .=  "$header" . ",";
			}
		}
		$csv .= $this->getLineSeperator();
		$rowcount = count($rows);
		foreach($rows as $r_key => $row){
			if ($r_key != ($rowcount - 1)){
				for ($i=0;$i<$header_count;$i++){
					if($i == ($header_count - 1)){
						$csv .= $row[$headers[$i]];
					}else{
						$csv .= $row[$headers[$i]] . ",";
					}
					
				}
				$csv .= $this->getLineSeperator();
			}else{
				for ($i=0;$i<$header_count;$i++){
					if($i == ($header_count - 1)){
						$csv .= $row[$headers[$i]];
					}else{
						$csv .= $row[$headers[$i]] . ",";
					}
				}
			}
		}

		return $csv;
			
	}

	public function getLineSeperator(){
		return $this->line_separator;
	}

	public function setLineSeparator($seperator){
		// default is a new-line char: '\n'
		$this->line_separator = $seperator;
	}
}




?>